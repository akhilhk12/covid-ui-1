export class Profiles {
    macid: string;
    username: string;
    firstname: string;
    lastname: string;
    phoneno: string;
    age: number;
    covidstat: string;
    dod: Date;
}
